ventanaDeInicio = Titanium.UI.currentWindow;

ventanaDeInicio.orientationModes = [Titanium.UI.PORTRAIT];
ventanaDeInicio.layout = "vertical";

var parse = require('parselogin');
var lectorPdf = require('org.ideostudio.pdf');
var countrevistas = 0;
var countscrollview = 0;
var arregloDeImagenes = [];
var archivoLocal;
var detalle;
var titulo;
var articulos;
var i = 0;
var limite = 1000;
var salto = 0;
var paginaActualDeParse = 1;
var banderaSonido = false;

var vistaBotones = Ti.UI.createView({
	layout: "horizontal",
	top: 0,
	height: 50,
	borderColor: '#000',
	borderWidth: 1,
	borderRadius: 1
});

vistaBotones.setBackgroundGradient( {
	type: 'linear',
	colors: ['#142949', '#142949'],
	backFillStart: false
});

var vistaBotonesDerecha = Ti.UI.createView({
	top: 5,
	layout: "horizontal",
	width: "50%"
});

var vistaBotonesIzquierda = Ti.UI.createView({
	top: 5,
	layout: "horizontal",
	width: "50%"
});

var vistaLista = Ti.UI.createView({
	layout: "horizontal",
	borderWidth: 3,
	backgroundColor: 'white',
	borderRadius: 1,
	borderColor: '#666',
	height: "25%",
	bottom: 0
});

var vistaImagen = Ti.UI.createView({
	backgroundImage: '../image_background.png',
	borderColor: '#000',
	borderWidth: 1,
	borderRadius: 1,
	height: "80%",
	bottom: "20%"
});

var vistaListaIzquierda = Ti.UI.createView({
	layout: "vertical",
	width: "50%"
});

var vistaListaDerecha = Ti.UI.createView({
	layout: "vertical",
	width: "50%"
});


var botonSalir = Titanium.UI.createButton({
	title: 'Salir',
	color: 'white',
	backgroundImage: '../boton_gris.png',
	height: 40,
	width: 200,
});

var botonEnglish = Titanium.UI.createButton({
	title: 'English',
	color: 'white',
	backgroundImage: '../boton_azul.png',
	height: 40,
	width: 100,
});

var botonEspanol = Titanium.UI.createButton({
	title: 'Español',
	color: 'white',
	backgroundImage: '../boton_azul.png',
	height: 40,
	width: 100,
});

var botonArchivo = Titanium.UI.createButton({
	title: 'Archivo',
	backgroundImage: '../boton_azul.png',
	color: 'white',
	height: 40,
	width: 100,
	right: 10,
});

var botonAccion = Titanium.UI.createButton({
	title: 'Leer',
	backgroundImage: '../boton_azul.png',
	color: 'white',
	height: 40,
	width: 200,
	bottom: 10,
    left: 20
});

var botonAudio = Titanium.UI.createButton({
	title: 'Escuchar',
	backgroundImage: '../boton_azul.png',
	height: 40,
	width: 200,
	color: 'white',
	top: '40%',
	right: 20,
});

var botonEliminar = Titanium.UI.createButton({
	title: 'Eliminar',
	backgroundImage: '../boton_azul.png',
	height: 40,
	width: 200,
	top: '20'
});

var botonSiguiente = Titanium.UI.createButton({
	backgroundImage: '../boton_siguiente.png',
	height: 40,
	width: 40,
});

var botonAnterior = Titanium.UI.createButton({
	backgroundImage: '../boton_anterior.png',
	height: 40,
	width: 40,
});


	botonSalir.title = 'Logout';
	botonAccion.title = 'Read';
	botonAudio.title = 'Listen';
	botonEliminar.title = "Delete";
	botonArchivo.title = 'Archive';

var textoDetalle = Ti.UI.createLabel({
	text: detalle,
	color: 'black',
	width: '90%',
	top: '20',
	left: '20',
	font: {
		fontSize: 16,
	},
	verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP
});

var textoTitulo = Ti.UI.createLabel({
	text: '',
	width: "90%",
	color: 'black',
	top: '20',
	left: '20',
	height: 24,
	font: {
		fontSize: 20,
		fontWeight: 'bold'
	}
});

var scrollView = Titanium.UI.createScrollableView({
	width: "100%",
	height: "100%",
	maxZoomScale: 1.0,
	showPagingControl: true,
	backgroundColor: "#808080",
});

var vistaBotonesArticulo = Ti.UI.createView({
    layout: "horizontal",
    backgroundColor: "white",
    height: "50px",
    width: "100%",
    bottom: "0"
});

vistaBotonesIzquierda.add(botonSalir);
vistaBotones.add(vistaBotonesIzquierda);
vistaBotones.add(vistaBotonesDerecha);

vistaBotonesArticulo.add(botonAccion);

ventanaDeInicio.add(scrollView);
ventanaDeInicio.add(vistaBotones);
ventanaDeInicio.add(vistaBotonesArticulo);

vistaListaDerecha.add(botonAccion);
vistaListaDerecha.add(botonEliminar);
vistaListaIzquierda.add(textoTitulo);
vistaListaIzquierda.add(textoDetalle);
vistaLista.add(vistaListaIzquierda);
vistaLista.add(vistaListaDerecha);
//ventanaDeInicio.add(vistaLista);

var banderapaginado = false;

var nuevaLogin = Titanium.UI.createWindow({
	url : '../app.js',
	title : 'inicio',
	width : '100%',
	height : '100%',
});

botonSalir.addEventListener('click', function() {
	ventanaDeInicio.close({
		view : ventanaDeInicio,
		opacity : 0,
		duration : 500
	});
	nuevaLogin.open({
		view : nuevaLogin,
		opacity : 100,
		duration : 500
	});
	if (banderaSonido) {
		sonido.stop();
		banderaSonido = false
	}
});

botonEnglish.addEventListener('click', function() {
	Ti.App.Properties.setString('lenguaje', 'en');
	botonSalir.title = 'Logout';
	botonArchivo.title = 'Archive';
	botonAccion.title = 'Read';
	botonAudio.title = 'Listen';
	botonEliminar.title = "Delete";
	botonEnglish.enabled = false;
	botonEspanol.enabled = true;
	salto = 0;
	i = 0;
	botonSiguiente.hide();
	botonAnterior.hide();
	cargaArticulos(limite, salto);
	if (banderaSonido) {
		sonido.stop();
		banderaSonido = false
	}
});

botonEspanol.addEventListener('click', function() {
	Ti.App.Properties.setString('lenguaje', 'es');
	botonSalir.title = 'Salir';
	botonArchivo.title = 'Archivo';
	botonAccion.title = 'Leer';
	botonAudio.title = 'Escuchar';
	botonEliminar.title = "Eliminar";
	botonEnglish.enabled = true;
	botonEspanol.enabled = false;
	salto = 0;
	i = 0;
	botonSiguiente.hide();
	botonAnterior.hide();
	cargaArticulos(limite, salto);
	if (banderaSonido) {
		sonido.stop();
		banderaSonido = false
	}
});

var nuevaVentana2 = Titanium.UI.createWindow({
	url : 'vistas/archivo.js',
	title : 'inicio',
	backgroundColor : 'white',
	width : '100%',
	height : '100%',
});

botonArchivo.addEventListener('click', function() {
	nuevaVentana2.open();
	if (banderaSonido) {
		sonido.stop();
		banderaSonido = false
	}
});

/**
scrollView.addEventListener('scroll', function(e) {
	i = e.currentPage;
	try {
		detalle = articulos[i].description;
		textoTitulo.text = articulos[i].title;
		textoDetalle.text = detalle;
		archivoLocal = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, articulos[i].file.url.substr(articulos[i].file.url.length - 11, articulos[i].file.url.length - 1))
		if (Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, articulos[i].screenname+'.pdf').exists()) {
			botonEliminar.enabled = true;
		} else {
			botonEliminar.enabled = false;
		}
	} catch(e) {
		
	}
	
	if (i == articulos.length - 1 && paginaActualDeParse < totalPaginas) {
		botonSiguiente.show();

	} else {
		botonSiguiente.hide();
	}

	if (i == 0 && paginaActualDeParse > 1) {
		botonAnterior.show();

	} else {
		botonAnterior.hide();
	}

	if (banderaSonido) {
		sonido.stop();
		banderaSonido = false
	}
});
**/

botonAccion.addEventListener('click', function(e) {
	var nombreArticulo = articulos[i].file.url.substr(articulos[i].file.url.length - 11, articulos[i].file.url.length - 1);
	var descargarArticulo = articulos[i].file.url;
	titulo = articulos[i].title;
	if (Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, articulos[i].file.url.substr(articulos[i].file.url.length - 11, articulos[i].file.url.length - 1)).exists() && (botonAccion.title == "Leer" || botonAccion.title == "Read")) {

		var pdfFile = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, nombreArticulo);

		Titanium.UI.setBackgroundColor('#000');

		lectorPdf.url = pdfFile.getNativePath();

		lectorPdf.openpdf(Ti.Filesystem.applicationDataDirectory + nombreArticulo);

	} else {
		botonAccion.enabled = false;


			botonAccion.title = "Downloading...";

		var xhr = Titanium.Network.createHTTPClient();
		xhr.onload = function() {
			var ruta = this.location.substr(this.location.length - 11, this.location.length - 1);
			var fecha = ruta.substr(0, 7);
			var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, ruta);
			f.write(this.responseData);
			archivoLocal = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, ruta)

			if (archivoLocal.exists()) {
				botonAccion.enabled = true;

					botonAccion.title = "Read";

				var lenguaje = "en";
				nombrearchivo = Titanium.Filesystem.applicationDataDirectory + ruta;
				var db = Titanium.Database.open('spurrierdb');
				db.execute("CREATE TABLE IF NOT EXISTS SPURRIER (LENGUAJE TEXT, TITULO TEXT, NOMBRE_ARCHIVO TEXT, FECHA TEXT)");
				db.execute("INSERT INTO SPURRIER (LENGUAJE, TITULO, NOMBRE_ARCHIVO, FECHA ) VALUES(?,?,?,?)", lenguaje, titulo, nombrearchivo, fecha);
				db.close();
				botonEliminar.enabled = true;
				Ti.API.info("document loaded in database");
			} else {
				botonAccion.title = "Downloading...";
			}

		};
		xhr.open('GET', articulos[i].file.url);
		xhr.send();

	}
});

botonAudio.addEventListener('click', function() {
	Ti.API.info('Estamos en la accion de escuchar audio... ' + articulos[i].audio.url.substr(articulos[i].audio.url.length - 11, articulos[i].audio.url.length - 1));
	var nombreAudio = articulos[i].screenname + '.mp3';

	titulo = articulos[i].title;

	sonido = Titanium.Media.createSound({
		url : articulos[i].audio.url
	});
	sonido.play();
	banderaSonido = true
});

botonEliminar.addEventListener('click', function(){
	if (Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, articulos[i].screenname+'.pdf').exists()) {
		var nombrearchivo = articulos[i].screenname+'.pdf';
		Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, articulos[i].screenname+'.pdf').deleteFile();
		botonEliminar.enabled = false;
	}
});

function cargaArticulos(arg1, arg2) {
	//vistaImagen.remove(scrollView);
	salto = arg2;
	i = scrollView.currentPage;
	
	if (scrollView.views) {

	};

	var avd = Ti.UI.createImageView({
		image: '',
		left: 0,
		height: 800,
		width: 580
	})
	arregloDeImagenes = [];

	var parse = require('parse');
	var client = new parse.Client();
	var whereClause = {
		"ln"	: Ti.App.Properties.getString('lenguaje')
	};

	client.get({
		className : 'Editions',
		payload : {
			"where" : JSON.stringify(whereClause),
			"count" : 1,
			"order": "-edition",	
			"limit" : limite,
			"skip" : salto
		},
		success : function(response) {
			try {
				articulos = response.results;
				vistasParaScroll = [];
				articulos.forEach(function(row, item) {
					Ti.API.info("Objetito json: >>>>>><<<<<<" + JSON.stringify(row));
					var tituloArticulo = Ti.UI.createLabel({
					    top: 10,
					    backgroundColor: "white",
						color: "black",
					    width: "100%",
					    left: "20px",
					    right: "20px",
						font: {
							fontSize: 21 //16
						},
						text: row.title,
					});
					
					var descripcionArticulo = Ti.UI.createLabel({
					    backgroundColor: "white",
						color: "black",
						width: "100%",
						left: "20px",
						font: {
							fontSize: 16
						},
						text: row.description
					});
					
					vistaArticulo = Ti.UI.createView({
					    layout: "vertical",
						borderColor: '#000',
						borderWidth: 1,
						borderRadius: 1
					});
					
			        vistaDetalle = Ti.UI.createView({
			            backgroundColor: "white",
			            width: "100%",
			            height: "100%"
			        });
					
					vistaDetalle.add(tituloArticulo);
					vistaDetalle.add(descripcionArticulo);

					archivoImagen = Ti.Filesystem.applicationDataDirectory + row.screenshot.name;
					
					var reduccionImagen = Ti.UI.create2DMatrix({ scale: 0.90 });				
					var imagenArticulo = Ti.UI.createImageView({
					    top: "5%",
					    width: "100%",
					    center: {x: "50%"},
						defaultImage: "image_placeholder.png",
						image: row.screenshot.url,
					});
					imagenArticulo.setTransform(reduccionImagen);
					
					vistaArticulo.add(imagenArticulo);
					vistaArticulo.add(vistaDetalle);
					vistasParaScroll.push(vistaArticulo);
				});
				scrollView.views = vistasParaScroll;
				/**
				totalPaginas = Math.ceil(response.count / limite);
				Ti.API.info(totalPaginas);
				countrevistas = articulos.length;
				banderita = true;
				countscrollview = 0;
				arregloDeImagenes = [null];

				for (var c = 0; c < response.results.length; c++) {

					var nombrearchivo = response.results[c].screenshot.url.substr(response.results[c].screenshot.url.length - 11, response.results[c].screenshot.url.length - 1);
					var archivoimagen = Titanium.Filesystem.applicationDataDirectory + nombrearchivo;
					var existeArchivo = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, nombrearchivo);
					if (existeArchivo.exists() == 0) {
						banderita = false;

						var nombrearchivo = response.results[c].screenname;

						var xhr = Titanium.Network.createHTTPClient();
						xhr.onload = function() {
							var ruta = this.location.substr(this.location.length - 11, this.location.length - 1);
							var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, ruta);
							f.write(this.responseData);

							countscrollview++;

							if (countrevistas == countscrollview) {
								scrollView.views = arregloDeImagenes;
							}

						};

						xhr.open('GET', response.results[c].screenshot.url);
						xhr.send();

						var m = Ti.UI.create2DMatrix({ scale: 0.70 });				
						var av = Ti.UI.createImageView({
							image: archivoimagen
						});
						av.setTransform(m);

						scrollView.currentPage = 0;
						textoDetalle.text = articulos[scrollView.currentPage].description;
						textoTitulo.text = articulos[scrollView.currentPage].title;

						arregloDeImagenes[c] = av;
						vistaImagen.add(scrollView);

					} else {

						scrollView.currentPage = 0;
						textoDetalle.text = articulos[scrollView.currentPage].description;
						textoTitulo.text = articulos[scrollView.currentPage].title;

						var m = Ti.UI.create2DMatrix({ scale: 0.70 });				
						var av = Ti.UI.createImageView({
							image: archivoimagen
						});
						av.setTransform(m);

						arregloDeImagenes[c] = av;
						countscrollview++;
						if (countrevistas == countscrollview) {
							scrollView.views = arregloDeImagenes;
							vistaImagen.add(scrollView);
						}

					};

				}
				scrollView.currentPage = 0;
				archivoLocal = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, nombrearchivo);
				**/
			} catch(E) {

			}
		},
		error : function(response, xhr) {

		}
	});

};
cargaArticulos(100, 0);
