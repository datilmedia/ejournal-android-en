

ventanaArchivo = Titanium.UI.currentWindow;

function getOrientation(o)
{  
	switch (o)
	{
		case Titanium.UI.PORTRAIT:
			return '1';
		case Titanium.UI.UPSIDE_PORTRAIT:
			return '2';
		case Titanium.UI.LANDSCAPE_LEFT:
			return '3';
		case Titanium.UI.LANDSCAPE_RIGHT:
			return '4';
		case Titanium.UI.FACE_UP:
			return 'face up';
		case Titanium.UI.FACE_DOWN:
			return 'face down';
		case Titanium.UI.UNKNOWN:
			return 'unknown';
	}
}

var lectorPdf = require('org.ideostudio.pdf');
var datos = [];
var tituloArchivo = [];
var ubicacionArchivo = [];
var fechaArchivo = [];

var archivoBotones = Titanium.UI.createView({
	backgroundImage :'../botones_background.png',
	width : '100%',
	top : 0
});

ventanaArchivo.add(archivoBotones);

var archivoCerrar = Ti.UI.createButton({
	width : 200,
	height : 40,
	color: 'white',
	backgroundImage:'../boton_azul.png',
	title : 'Cerrar'
});		

if(Ti.App.Properties.getString('lenguaje') == 'es'){
	archivoCerrar.title = 'Cerrar';
}else{
	archivoCerrar.title = 'Close';
};

function cargaTablaBaseDeDatos() {
 datos = [];
 tituloArchivo = [];
 ubicacionArchivo = [];
 fechaArchivo = [];
	
	var db = Titanium.Database.open('spurrierdb');
	db.execute("CREATE TABLE IF NOT EXISTS SPURRIER (LENGUAJE TEXT, TITULO TEXT, NOMBRE_ARCHIVO TEXT, FECHA TEXT)");
	
	if(Ti.App.Properties.getString('lenguaje') == 'es'){
		var archivos = db.execute('SELECT * FROM SPURRIER WHERE LENGUAJE = "es"');
	}else{
		var archivos = db.execute('SELECT * FROM SPURRIER WHERE LENGUAJE = "en"');
	}
	
	var contador = 0;
	var numeroTotalDeFilas = archivos.getRowCount();
	Ti.API.info('TOTAL DE FILAS DEVUELTAS: '+numeroTotalDeFilas);
	while(archivos.isValidRow()) {
	
		tituloArchivo.push({
			title : archivos.fieldByName('titulo')
		});
		ubicacionArchivo.push({
			ubicacion : archivos.fieldByName('nombre_archivo')
		})
		
		fechaArchivo.push({
			fecha : archivos.fieldByName('fecha')
		})
		
		archivos.next();
	
	}
	
	archivos.close();
	db.close();
	
	for (var c=0;c<tituloArchivo.length;c++){
		
		var bgcolor = (c % 2) == 0 ? '#fff' : '#dff2ff';
		var filaDeArchivo = Ti.UI.createTableViewRow({hasChild:false,height:40,backgroundColor:bgcolor});
		
		var vistaFila = Ti.UI.createView({
			height:'100%',
			layout:'vertical',
			left:5,
			top:5,
			bottom:5,
			right:5
		});
		
		var etiquetaDeTitulo = Ti.UI.createLabel({
			text:tituloArchivo[c].title,
			left:54,
			width:600,
			top:1,
			bottom:2,
			height:25,
			textAlign:'left',
			color:'#444444',
			font:{
				fontFamily:'Trebuchet MS',
				fontSize:18,
				fontWeight:'bold'
			}
		});
		vistaFila.add(etiquetaDeTitulo);
		
		var etiquetaDeFecha = Ti.UI.createLabel({
			text:fechaArchivo[c].fecha,
			left:54,
			width:120,
			top:1,
			bottom:2,
			height:18,
			textAlign:'left',
			color:'#444444',
			font:{
				fontFamily:'Trebuchet MS',
				fontSize:16,
			}
		});
		vistaFila.add(etiquetaDeFecha);
		filaDeArchivo.add(vistaFila)
		datos[c] = filaDeArchivo;
	}return datos;
}

var tablaDeArchivos = Titanium.UI.createTableView({
	data : '',
	editable : true,
	deleteButtonTitle : 'Eliminar',
	minRowHeight : 58,
});
ventanaArchivo.add(tablaDeArchivos);

if(Ti.App.Properties.getString('lenguaje') == 'es'){
	tablaDeArchivos.deleteButtonTitle = "Eliminar";
}else{
	tablaDeArchivos.deleteButtonTitle = "Delete";
};

tablaDeArchivos.data = cargaTablaBaseDeDatos();

var eliminandoDatos = Ti.UI.createView({
    backgroundColor: 'black',
    opacity: 0.75,
    height: '100%',
    width: '100%'
});

var ventanaDeConsultaEliminacion = Ti.UI.createView({
	borderWidth : 5,
	backgroundColor:'black',
	borderRadius : 2,
	borderColor : '#666',
    opacity: 0.75,
    height: '30%',
    width: '35%'
})
	
var alerta = Ti.UI.createImageView({
	image : '../alert.png',
	top : '10%',
	height : '30%',
	width : '15%'
})	
	
var textoDeConfirmacion = Ti.UI.createLabel({
text : '¿Esta seguro que desea eliminar esta edición?',
left : '15%',
width : '70%',
top : '45%',
height : '20%',
textAlign : 'left',
color : '#444444',
font : {
	fontFamily : 'Trebuchet MS',
	fontSize : '14%',
	fontWeight : 'bold'
	}
});
	
var eliminandoDatosAceptar = Ti.UI.createButton({
	width : '40%',
	height : '20%',
	color: '#424242',
	title : 'Aceptar',
	left : '7%',
	top : '70%',
	font : {
	fontFamily : 'Trebuchet MS',
	fontWeight : 'bold'
	}
});	

var eliminandoDatosCancelar = Ti.UI.createButton({
	width : '40%',
	height : '20%',
	color: '#424242',
	title : 'Cancelar',
	right : '7%',
	top : '70%',
	font : {
	fontFamily : 'Trebuchet MS',
	fontWeight : 'bold'
	}
});	

ventanaDeConsultaEliminacion.add(alerta);
ventanaDeConsultaEliminacion.add(textoDeConfirmacion);
ventanaDeConsultaEliminacion.add(eliminandoDatosAceptar);
ventanaDeConsultaEliminacion.add(eliminandoDatosCancelar);
eliminandoDatos.add(ventanaDeConsultaEliminacion);

if(getOrientation(Titanium.Gesture.orientation) == 1 || getOrientation(Titanium.Gesture.orientation) == 2){
	archivoBotones.height = '5.5%'; 
	archivoCerrar.top = '20%'; 
	archivoCerrar.left = '3%';
	tablaDeArchivos.top = '5.5%'
}

if(getOrientation(Titanium.Gesture.orientation) == 3 || getOrientation(Titanium.Gesture.orientation) == 4){
	archivoBotones.height = '9%'; 
	archivoCerrar.top = '20%'; 
	archivoCerrar.left = '1.8%';
	tablaDeArchivos.top = '9%'
}

Titanium.Gesture.addEventListener('orientationchange', function(e){
if(getOrientation(Titanium.Gesture.orientation) == 1 || getOrientation(Titanium.Gesture.orientation) == 2){
	archivoBotones.height = '5.5%'; 
	archivoCerrar.top = '20%'; 
	archivoCerrar.left = '3%';
	tablaDeArchivos.top = '5.5%'
}
if(getOrientation(Titanium.Gesture.orientation) == 3 || getOrientation(Titanium.Gesture.orientation) == 4){
	archivoBotones.height = '9%'; 
	archivoCerrar.top = '20%'; 
	archivoCerrar.left = '1.8%';
	tablaDeArchivos.top = '9%'
}
});

var a; 	

tablaDeArchivos.addEventListener('longclick', function(e)
{
	ventanaArchivo.add(eliminandoDatos);
	 a = e.index;
});

eliminandoDatosAceptar.addEventListener('click', function()
	{
			var db = Titanium.Database.open('spurrierdb');
			try {
				Ti.API.info('FECHAARCHIVO: '+fechaArchivo[a].fecha);
				db.execute('DELETE FROM SPURRIER WHERE FECHA = ?',fechaArchivo[a].fecha);
				db.close();
				Ti.API.info('TITULO: '+fechaArchivo[a].fecha);
				var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,fechaArchivo[a].fecha + '.pdf');
			
				if (file.exists()) { 
					
					file.deleteFile(); 
					a = 0; 
					ventanaArchivo.remove(tablaDeArchivos);
					tablaDeArchivos.data = cargaTablaBaseDeDatos();
					ventanaArchivo.add(tablaDeArchivos);
					ventanaArchivo.remove(eliminandoDatos);
				}
			} catch (E) {
			}

	});

eliminandoDatosCancelar.addEventListener('click', function(e)
{
	ventanaArchivo.remove(eliminandoDatos);
});

tablaDeArchivos.addEventListener('click', function(e)
{
	
	Titanium.UI.setBackgroundColor('#000');
	var pdfFile = Ti.Filesystem.getFile(ubicacionArchivo[e.index].ubicacion);
			
				lectorPdf.url = pdfFile.getNativePath() ;		

						Titanium.UI.setBackgroundColor('#000');
						
						lectorPdf.url = pdfFile.getNativePath() ;		
								    
			   			lectorPdf.openpdf();

});

archivoCerrar.addEventListener('click', function() {
	ventanaArchivo.close();
});
archivoBotones.add(archivoCerrar);



